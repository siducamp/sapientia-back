const express = require('express');
const app = express();
const https = require('https');
const fs = require('fs');
const bodyParser = require('body-parser');
const cors = require('cors');

const port = 3000;

const privateKey = fs.readFileSync('/etc/letsencrypt/live/api.learn-sapientia.com/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/api.learn-sapientia.com/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/api.learn-sapientia.com/chain.pem', 'utf8');

const options = {
  key : privateKey,
  cert : certificate,
  ca : ca
}

const server = https.createServer(options, app);

app.use(bodyParser.json());
app.use(cors());

server.listen(port, () => {
  console.log(`\n[${new Date().toLocaleString()}]`);
  console.log(`Server listening on port ${port}`);
})

const matrixDiag = require('./exercises/001-MatrixDiagonalization/matrixDiag.js');

app.post('/exercises/matrix-diagonalization', async function(req, res) {
  let result;
  try {
    console.log(`\n[${new Date().toLocaleString()}]`);
    console.log("Received request : ", JSON.stringify(req.body));
    result = await matrixDiag(req.body);
    res.status(200);
  } catch (error) {
    console.log(error);
    res.status(400);
  }
  console.log(result);
  res.send(result);
});